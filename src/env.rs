use std::fmt::{Display, Formatter};
use std::collections::HashMap;
use std::rc::Rc;

use super::sexp::Sexp;

pub struct Env(pub HashMap<String, Rc<Sexp>>);

impl Clone for Env {
    fn clone(&self) -> Self {
        Env(self.0.clone())
    }
}

impl Display for Env {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for (k, v) in &self.0 { write!(f, "{}: {}, ", k, *v)?; }
        Ok(())
    }
}

impl Env {
    pub fn empty() -> Env {
        Env(HashMap::new())
    }
    pub fn eval_str(&mut self, text: &str) -> Sexp {
        let mut iter = text.chars().peekable();
        let mut result = Sexp::Nil;
        while iter.peek() != None {
            match Sexp::parse(&mut iter) {
                Some(sexp) => result = sexp.evaluate(self),
                None => continue,
            }
        }
        result
    }
}

impl Default for Env {
    fn default() -> Env {
        let mut env = Self::empty();

        env.0.insert("+".to_string(), Rc::new(Sexp::Proc(Rc::new(sum))));
        env.0.insert("*".to_string(), Rc::new(Sexp::Proc(Rc::new(prod))));
        env.0.insert(">".to_string(), Rc::new(Sexp::Proc(Rc::new(greater_than))));
        env.0.insert("<".to_string(), Rc::new(Sexp::Proc(Rc::new(less_than))));
        env.0.insert("eval".to_string(), Rc::new(Sexp::Proc(Rc::new(eval))));
        env.0.insert("cons".to_string(), Rc::new(Sexp::Proc(Rc::new(cons))));
        env.0.insert("car".to_string(), Rc::new(Sexp::Proc(Rc::new(car))));
        env.0.insert("cdr".to_string(), Rc::new(Sexp::Proc(Rc::new(cdr))));
        env.0.insert("eq?".to_string(), Rc::new(Sexp::Proc(Rc::new(equal))));
        env.0.insert("display".to_string(), Rc::new(Sexp::Proc(Rc::new(display))));
        env
    }
}

fn sum(input: &[Sexp], env: &mut Env) -> Sexp {
    let mut result = 0i64;
    for val in input {
        result += val.evaluate_to_number(env);
    }
    Sexp::Number(result)
}
fn prod(input: &[Sexp], env: &mut Env) -> Sexp {
    let mut result = 1i64;
    for val in input {
        result *= val.evaluate_to_number(env);
    }
    Sexp::Number(result)
}
fn greater_than(input: &[Sexp], env: &mut Env) -> Sexp {
    let left = input[0].evaluate_to_number(env);
    let right = input[1].evaluate_to_number(env);
    Sexp::Number(if left > right { 1i64 } else { 0i64})
}
fn less_than(input: &[Sexp], env: &mut Env) -> Sexp {
    let left = input[0].evaluate_to_number(env);
    let right = input[1].evaluate_to_number(env);
    Sexp::Number(if left < right { 1i64 } else { 0i64})
}
/// this version of eq? is more simplistic than the usual
/// both oprands must be the same type, and are based on rusts comparables
fn equal(input: &[Sexp], _: &mut Env) -> Sexp {
    let first = &input[0];
    let second = &input[1];

    if first == second { Sexp::Number(1) } else { Sexp::Number(0) }
}
/// given an argument ta the 0th position, return that as a result
fn eval(input: &[Sexp], env: &mut Env) -> Sexp {
    if let Sexp::List(list) = &input[0] {
        if let Sexp::Symbol(sym) = &list[0] {
            if sym == "'" {
                return list[1].evaluate(env)
            }
        }
    }
    input[0].evaluate(env)
}
/// concat an element and a list
fn cons(input: &[Sexp], _: &mut Env) -> Sexp {
    if input.len() != 2 {
        panic!("cons expects exactly 2 arguments: element + list");
    }
    let mut result = vec![input[0].clone()];
    if let Sexp::List(mut l) = input[1].clone() {
        result.append(&mut l);
        Sexp::List(result)
    } else {
        panic!("cons expected its second arg as a list")
    }
}
// return the first element of a list
fn car(input: &[Sexp], _: &mut Env) -> Sexp {
    match &input[0] {
        Sexp::List(l) => l[0].clone(),
        _ => panic!("invalid input to car"),
    }
}
// return the tail of a list (cdr '(x y z)) -> (y z)
fn cdr(input: &[Sexp], _: &mut Env) -> Sexp {
    match &input[0] {
        Sexp::List(l) if l.len() <= 1 => Sexp::List(Vec::new()),
        Sexp::List(l) => Sexp::List(l[1..].iter().map(|e| e.clone()).collect()),
        _ => panic!("invalid form passed to cdr")
    }
}
/// print a value into the console
fn display(input: &[Sexp], _: &mut Env) -> Sexp {
    println!("{}", input[0]);
    Sexp::Nil
}

#[cfg(test)]
mod tests {
    use bee::{Env, Sexp};

    #[test]
    fn test_closures() {
        let mut env = Env::default();
        let result = env.eval_str(
        "(def adder (fn (x) (fn (y) (+ x y))))
            (def add-100 (adder 100))
            (add-100 11)");
        match result {
            Sexp::Number(n) => assert_eq!(111.0, n),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_list_manipulation() {
        let expected = vec![
            Sexp::Symbol("nobody".to_string()),
            Sexp::Symbol("and".to_string())];
        let mut env = Env::default();
        let result = env.eval_str("(cdr (car (cons '(for nobody and) '(no one))))");
        match result {
            Sexp::List(l) => assert!(expected == l),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_if_expr() {
        let mut env = Env::default();
        let result = env.eval_str(
            "(def hello (if (eq? 0 0) 'for 'me))");
        match result {
            Sexp::Symbol(s) => assert_eq!("for", s),
            _ => assert!(false),
        }
    }

    #[test]
    fn test_cond() {
        let mut env = Env::default();
        let result = env.eval_str("
        (cond
         ((eq? 'hello 'world) 'hello-world)
         ((eq? 1 1) 'Zero!))");
        match result {
            Sexp::Symbol(s) => assert_eq!("Zero!", s),
            _ => assert!(false),
        }
        let result = env.eval_str("
        (cond
         ((eq? 'hello 'hello) 'hello-world)
         ((eq? 1 1) 'Zero!))");
        match result {
            Sexp::Symbol(s) => assert_eq!("hello-world", s),
            _ => assert!(false),
        }
        let result = env.eval_str("
        (cond
         ((eq? 'hello 'world) 'hello-world)
         ((eq? 1 0) 'Zero!))");
        match result {
            Sexp::Nil => assert!(true),
            _ => assert!(false),
        }
    }
}