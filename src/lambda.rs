use std::rc::Rc;

use super::env::Env;
use super::sexp::*;

pub struct Lambda {
    // the lambdas parameters
    pub param_names: Vec<String>,
    // the expression that this lambda will evaluate
    pub expr: Sexp,
    // the environment that was present when the lambda was created
    pub env: Env
}

impl Lambda {
    pub fn call(&self, args: &[Sexp]) -> Sexp {
        //println!("env pre-arg setup: {}", env);
        let mut env = self.env.clone();
        for (name, val) in self.param_names.iter().zip(args) {
            let val = val.evaluate(&mut env);
            env.0.insert(name.clone(), Rc::new(val));
        }
        //println!("env pre-call: {}", env);
        let result = self.expr.evaluate(&mut env);
        //println!("env post call: {}", env);
        result
    }
}

impl Default for Lambda {
    fn default() -> Self {
        Lambda {
            param_names: Vec::new(),
            expr: Sexp::Nil,
            env: Env::default(),
        }
    }
}