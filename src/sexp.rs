use std::borrow::Borrow;
use std::fmt::{Display, Formatter};
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use std::iter::Peekable;
use std::str::Chars;
use peeking_take_while::PeekableExt;

use super::env::*;
use super::lambda::*;

const EMPTY_ARGS: &[Sexp] = &[];

pub enum Sexp {
    // a symbol is a type of identifier
    // if it is encountered alone or as part of a list it will be looked up
    // if it is the first element of a list it will be looked up in an effort to peform
    // an invokation
    Symbol(String),
    // a plain number
    Number(i64),
    // a list, unless quoted if the first element
    // is symbol will be evaluated
    List(Vec<Self>),
    // an environment containing variable names and expressions
    Env(Env),
    // A user supplied or library supplied proc that the ast can evaluate
    Proc(Rc<dyn Fn(&[Sexp], &mut Env)->Sexp>),
    Lambda(Rc<Lambda>),
    Nil,
}

impl Clone for Sexp {
    fn clone(&self) -> Self {
        match self {
            Self::Symbol(s) => Self::Symbol(s.clone()),
            Self::Number(n) => Self::Number(*n),
            Self::List(l) => Self::List(l.clone()),
            Self::Env(e) => Self::Env(e.clone()),
            Self::Proc(p) => Self::Proc(p.clone()),
            Self::Lambda(l) => Self::Lambda(l.clone()),
            Self::Nil => Self::Nil,
        }
    }
}

impl Display for Sexp {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Symbol(s) => write!(f, "{}", s),
            Self::Number(s) => write!(f, "{}", s),
            Self::List(l) => {
                write!(f, "(")?;
                for s in l { write!(f, "{} ", s)?; }
                write!(f, ")")?;
                Ok(())
            },
            Self::Env(e) => {
                for (k, v) in &e.0 { write!(f, "{}: {}", k, v)?; }
                Ok(())
            },
            Self::Proc(_) => write!(f, "procedure"),
            Self::Lambda(l) => {
                let mut params = String::new();
                l.param_names.iter().for_each(|n| params += &(n.to_owned() + ", "));
                write!(f, "fn ({}) ({})", params, l.expr)
            },
            Self::Nil => write!(f, "nil"),
        }
    }
}

impl PartialEq for Sexp {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Self::Symbol(s) => match other {
                Self::Symbol(o) => s == o,
                _ => panic!("invalid comparison Sexp::Symbol/other"),
            },
            Self::Number(n) => match other {
                Self::Number(o) => n == o,
                _ => panic!("invalid comparison Sexp::Number/other"),
            },
            Self::List(l) => match other {
                Self::List(o) => l == o,
                _ => panic!("invalid comparison Sexp::List/other"),
            },
            Self::Nil => match other {
                Self::Nil => true,
                _ => false,
            },
            Self::Env(e) => match other {
                Self::Env(o) => e.0 == o.0,
                _ => panic!("invalid comparison Sexp::Env/other"),
            },
            _ => panic!("non comparable type tested"),
        }
    }
}

impl Hash for Sexp {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Self::Number(n) => n.hash(state),
            Self::Symbol(s) => s.hash(state),
            Self::List(l) => l.iter().for_each(|v| v.hash(state)),
            _ => unimplemented!(),
        }
    }
}

impl Sexp {
    pub fn evaluate(&self, env: &mut Env) -> Self {
        match self {
            Self::Symbol(sym) => env.0.get(sym).expect(&format!("could not find {}", sym)).as_ref().clone(),
            Self::Number(n) => Self::Number(*n),
            Self::List(list) => Self::evaluate_list(&list, env),
            Self::Lambda(l) => l.call(&[]),
            _ => unimplemented!("UH OH!"),
        }
    }
    fn evaluate_list(list: &Vec<Self>, env: &mut Env) -> Self {
        // unwrap the first symbol:
        let first = &list[0];
        match first {
            Self::Symbol(sym) if sym == "fn" => {
                let args = match &list[1] {
                    Sexp::List(l) => l,
                    _ => panic!("expected a list of symbols")
                };
                let mut param_names = Vec::new();
                for arg in args {
                    if let Sexp::Symbol(s) = arg {
                        param_names.push(s.clone());
                    } else {
                        panic!("expected symbol in argument list");
                    }
                }
                return Sexp::Lambda(Rc::new(Lambda{param_names, env: env.clone(), expr: list[2].clone()}))
            },
            // if evaluates the 2nd element of the list, and returns the 3rd element if 2nd is true, otherwise 4th
            Self::Symbol(sym) if sym == "if" => {
                let test = &list[1];
                let conseq = &list[2];
                let alt = &list[3];
                let test_result = test.evaluate(env);
                match test_result {
                    Self::Number(num) if num != 0i64 => conseq.evaluate(env),
                    Self::Number(_) => alt.evaluate(env),
                    _=> panic!("invalid conditional"),
                }
            },
            Self::Symbol(def) if def == "def" => {
                let sym = match &list[1] {
                    Self::Symbol(name) => name,
                    _ => panic!("Expected a symbol to be passed to define"),
                };
                let result = list[2].evaluate(env);
                env.0.insert(sym.clone(), Rc::new(result.clone()));
                return result;
            },
            Self::Symbol(def) if def == "'" => {
                return list[1].clone()
            },
            // cond checks a list of conditionals until one evaluates true, and returns the first action that does
            Self::Symbol(cond) if cond == "cond" => {
                for conditional in &list[1..] {
                    // element 0 is the conditional - if it evaluates 1.0 then return element 1
                    match conditional {
                        Sexp::List(list) if list[0].evaluate_to_number(env) == 1 =>
                            return list[1].evaluate(env),
                        _ => continue,
                    }
                }
                return Sexp::Nil
            },
            Self::Symbol(hasher) if hasher == "#" => {
                // for each item in list - make key value pairs and return em as an object
                let interior_list = match &list[1] {
                    Sexp::List(interior_list) => interior_list,
                    _ => panic!("expected list of pairs"),
                };
                for pair in interior_list {

                }
                return Self::Nil
            },
            Self::Symbol(proc) => Self::proc_call(&proc, list, env),
            Self::Lambda(l) => {
                let args = if list.len() > 1 { &list[1..] } else { EMPTY_ARGS };
                return l.call(args)
            },
            Self::List(l) => {
                let proc = Self::evaluate_list(&l, env);
                let args = if list.len() > 1 { &list[1..] } else { EMPTY_ARGS };
                match proc.borrow() {
                    Sexp::Lambda(l) => return l.call(args),
                    _ => panic!("could not call proc")
                }
            },
            _ => panic!("UHOH!")
        }
    }
    pub fn evaluate_to_number(&self, env: &mut Env) -> i64 {
        match self {
            Self::Number(num) => *num,
            _ => match self.evaluate(env) {
                Self::Number(num) => num,
                _ => panic!("could not evaluate to a number, env: {}", env),
            }
        }
    }
    fn proc_call(proc: &str, proc_args: &Vec<Self>, env: &mut Env) -> Self {
        //println!("Calling proc: {}", proc);
        // the arguments, if any - start at one
        let proc = env.0.get(proc).expect(&format!("unknown proc {}", proc)).clone();
        let args = if proc_args.len() > 1 {
            proc_args[1..].iter().map(|e| e.evaluate(env)).collect::<Vec<_>>()
        } else { Vec::new() };
        match proc.borrow() {
            Self::Proc(fun) => fun(&args, env),
            Self::Lambda(l) => l.call(&args),
            _ => panic!("invalid proc call"),
        }
    }

    pub fn parse(iter: &mut Peekable<Chars>) -> Option<Sexp> {
        let peeked = iter.peek().unwrap();
        if *peeked == '(' {
            iter.next();
            let mut result = Vec::new();
            while iter.peek() != None && *iter.peek().unwrap() != ')' {
                match Self::parse(iter) {
                    Some(sexp) => result.push(sexp),
                    None => continue,
                }
            }
            // take the )
            iter.next();
            return Some(Sexp::List(result))
        } else if peeked.is_numeric() {
            let number = iter
                .peeking_take_while(|ch| ch.is_numeric() || *ch == '.')
                .collect::<String>()
                .parse::<i64>().unwrap();
            return Some(Sexp::Number(number))
        } else if peeked.is_alphabetic() {
            let symbol = iter
                .peeking_take_while(|ch| ch.is_alphanumeric() || *ch == '-' || *ch == '_' || *ch == '?' || *ch == '!')
                .collect::<String>();
            return Some(Sexp::Symbol(symbol))
        } else if *peeked == '\'' {
            iter.next();
            let result = vec![
                Sexp::Symbol("'".to_string()),
                Self::parse(iter).unwrap()];
            return Some(Sexp::List(result))
        } else if *peeked == '#' {
            iter.next();
            let result = vec![
                Sexp::Symbol("#".to_string()),
                Self::parse(iter).unwrap()];
            return Some(Sexp::List(result))
        } else if peeked.is_whitespace() {
            // Consume all whitespace currently in our way
            iter.peeking_take_while(|ch| ch.is_whitespace()).for_each(|_| {});
            return None
        } else if *peeked == ';' {
            // Consume until the end of line - the actual end of line can be handled by the previous if statement
            iter.peeking_take_while(|ch| *ch != '\n').for_each(|_| {});
            return None
        } else {
            let symbol = iter.peeking_take_while(|ch| !ch.is_alphanumeric() && !ch.is_whitespace())
                .collect::<String>();
            return Some(Sexp::Symbol(symbol))
        }
    }
}