mod env;
mod sexp;
mod lambda;

use env::*;

fn main() {
    let mut default_env: Env = Default::default();
    let result = default_env.eval_str(
        "
        ; define hello as the result of checking if 0 is equal to 0
(display (cond
 ((eq? 'hello 'hello) 'hello-world)
 ((eq? 1 1) 'Zero!))
)");
}
